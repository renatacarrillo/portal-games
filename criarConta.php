<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II">
    <meta name="author" content="Sthefany Pereira Nolasco de Souza | @contato: sthefany.nolasco22@gmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body class="conta color-background-gradient">
    <div>
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';

                    // breadcrumb
                    if (isset($_POST['PT'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', '' =>'Simuladores'));
                    } elseif (isset($_POST['EN'])) {
                        breadcrumb(array('index.php' => 'Home', '' =>'Simulator Games'));
                    } else {
                        breadcrumb(array('index.php' => 'Página Inicial', '' =>'Simuladores'));
                    }
                    
                    include('includes/lang.php');  
            ?>
            </div>
        </header>
        <div class="container mt-4 mb-5">
            <article class="border border-white rounded p-4">
                <h2>Cadastro</h2>
                <form>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Senha</label>
                                <input type="password" class="form-control" id="password" placeholder="Senha">
                            </div>
                            <div class="form-group col-md-4">
                                <img src="images/favicon2.png" height="50" alt="logo">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nome</label>
                                <input type="email" class="form-control" id="nome" placeholder="Ex: João">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Sobrenome</label>
                                <input type="password" class="form-control" id="sobrenome" placeholder="Ex:Silva">
                            </div>
                        </div>


                        <div class="form-row">
                        <div class="form-group col-md-4">
                                <label for="inputCity">Cidade :</label>
                                <input type="text" class="form-control" id="inputCity">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputEstado">Estado :</label>
                                <select id="inputEstado" class="form-control">
                                    <option selected>Escolher...</option>
                                    <option>AC - Acre</option>
                                    <option>AL - Alagoas</option>
                                    <option>AP - Amapá</option>
                                    <option>AM - Amazonas</option>
                                    <option>BA - Bahia</option>
                                    <option>CE - Ceará</option>
                                    <option>DF - Distrito Federal</option>
                                    <option>RJ - Rio de Janeiro</option>
                                    <option>RS - Rio Grande do Sul</option>
                                    <option>MG - Minas Geais</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-row">
                        <div class="form-group col-md-4">
                                <label>Data de Nascimento</label>
                                <input type="date" class="form-control" id="nascimentoDate" placeholder=""
                                    mask="00/00/00">
                            </div>
                            <div class="form-group col-md-2">
                                <label>CEP:</label>
                                <input type="text" class="form-control" id="cep" placeholder="" data-onlynumbers="true">
                            </div>
                            <div class="form-group col-md-5">
                                <label>Rua: </label>
                                <input type="text" class="form-control" id="street"
                                    placeholder="Apartamento, hotel, casa, etc.">
                            </div>
                            <div class="form-group col-md-1">
                                <label>Numero:</label>
                                <input type="text" class="form-control" id="numberHouse" placeholder=""
                                    data-onlynumbers="true">
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button type="submit" class="text-center btn btn-primary">Enviar</button>
                        </div>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </article>

    </div>
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>