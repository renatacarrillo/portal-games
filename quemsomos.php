<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 'Quem Somos'">
    <meta name="author" content="LUCAS JUSTI | @contato: lucas.silva563@fatec.sp.gov.br">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Quem Somos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body class="color-background-gradient">
    <div class="contatos">
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php' => 'Home', '' =>'Contatos'));

                    include('includes/lang.php');  
            ?>
            </div>
        </header>
        <article>
            <div class="container marketing">
            <hr class="hr">
                <h1 class="display-4 text-center mt-4 mb-5"><?php echo $aLang['123']; ?></h1>

                <!-- Three columns of text below the carousel -->
                <div class="row ">
                    <div class="col-lg-4">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-10">
                            <img class="rounded-circle justify-content-center" src="images/profile.png" alt="Generic placeholder image" width="140" height="140">
                        </div>
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-12">
                            <h2>Heading</h2>
                            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                                ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                                Praesent commodo cursus magna.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec
                            elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus
                            commodo, tortor mauris condimentum nibh.</p>

                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum
                            id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                            ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                            Praesent commodo cursus magna.</p>

                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec
                            elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus
                            commodo, tortor mauris condimentum nibh.</p>

                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum
                            id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                            ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                            Praesent commodo cursus magna.</p>

                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded-circle" src="images/profile.png" alt="Generic placeholder image" width="140"
                            height="140">
                        <h2>Heading</h2>
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec
                            elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus
                            commodo, tortor mauris condimentum nibh.</p>

                    </div><!-- /.col-lg-4 -->

                </div>
            </div>
        </article>
        <!--Footer-->
        <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
    </div>
</body>

</html>