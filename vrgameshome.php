<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II">
    <meta name="author" content="Sthefany Pereira Nolasco de Souza | @contato: sthefany.nolasco22@gmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body class="vrgames">
    <div>
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';

                    // breadcrumb
                    if (isset($_POST['PT'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', '' =>'Simuladores'));
                    } elseif (isset($_POST['EN'])) {
                        breadcrumb(array('index.php' => 'Home', '' =>'Simulator Games'));
                    } else {
                        breadcrumb(array('index.php' => 'Página Inicial', '' =>'Simuladores'));
                    }
                    
                    include('includes/lang.php');  
            ?>
            </div>
        </header>

        <article class="container">
            <hr class="hr">
            <div class="row mt-5">
                <div class="col-lg-8">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="p-0 m-1 mb-3">
                                    <div>
                                        <h1 class="mb-1">O FUTURO DOS JOGOS DIGITAIS</h1>
                                        
                                        <p>O mundo dos videogames está prestes a passar por mudanças sísmicas. Prepare-se para o surgimento de uma nova dimensão no entretenimento, pois a realidade virtual nos jogos (RV) pode mudar para sempre a forma como os imaginamos.</p>

                              
                                        <p>Às vezes aparece uma nova forma de tecnologia com o poder de revolucionar o mercado. É certamente o caso da realidade virtual, que, embora ainda recente, poderia elevar os jogos online a um novo patamar.

                                        É por isso que muitas das maiores marcas de tecnologia do mercado estão se interessando tanto pela RV e concorrendo umas com as outras rumo a esta inovação que vai se tornar padrão não só no setor dos jogos, mas também em outros setores.
                                        </p>
                                        
                                    </div>
                                    <hr class="mt-4 mb-4">
                                    <div class="row p-0 m-0" style="background:;">
                                        <div class="col-lg-12 pl-1 pb-0 pt-0">
                                            <h3><span class="place"></span> O que são jogos de realidade virtual?</h3>
                                            
                                        </div>
                                         <p>
                                        Em suma, os jogos de RV permitem que você tenha uma experiência imersiva de jogo; ao jogar, você tem a sensação de estar num mundo tridimensional. Embora jogar seja, em geral, uma forma de entretenimento bem absorvente, há uma certa distância pelo fato de o jogador estar diante da tela do dispositivo utilizado.
                                        </p>
                                        
                                        
                                        <div class="col-lg-6 pl-1 pb-0 pt-0">
                                            <div class="m-0 mb-3 p-0">
                                                <p style="background: ; margin-bottom: -15px; padding-bottom: 0px;">
                                                 Os jogos de RV tentam remover essa distância e põem o jogador no centro da ação. Para tanto, é necessário usar algum tipo de headset de RV, e peças de vestuário, como luvas com sensores integrados. Assim, os movimentos do jogador podem ser detectados, e ele se torna parte do jogo.   
                                                </p>
                                              
                                            </div>
                                        </div>
                                        <div class="col-lg-6 blockimg allimg p-0" style="">
                                            <img src="images/sthefany/virtual.jpg" class="rounded h-100" alt="teste"
                                                style="max-width: 100%; max-heght:100%;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <aside class="col-lg-4 p-0 mt-1" style="height: auto;">
                    <div class="col p-0 asideblock">
                        <div class="p-3" style="background: rgba(0,0,0,0.7);">
                            <h3 class="text-center pb-3">Os melhores jogos do PlayStation VR de 2019 até agora</h3>
                            <div class="row pl-2 pr-2 mt-2 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>Blood & Truth</h4>
                                   
                                    <p>Abra o caminho a tiros no submundo de Londres em um espetacular thriller de ação para o PlayStation VR.</p>
                                    <a href="https://www.playstation.com/pt-br/games/blood-and-truth-ps4/">Saiba Mais</a>
                                </div>
                            </div>
                            <div class="row pl-2 pr-2 mt-3 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>Everybody's Golf VR</h4>
                              
                                    <p>Entre nos campos de uma forma nunca antes vista no jogo mais envolvente da série Everybody’s Golf!</p>
                                    <a href="https://www.playstation.com/pt-br/games/everybodys-golf-vr-ps4/">Saiba Mais</a>
                                </div>
                            </div>
                            <div class="row pl-2 pr-2 mt-3 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>Immortal Legacy: The Jade Cipher</h4>
                                   
                                    <p>É melhor deixar alguns mistérios enterrados… Descubra a verdade perturbadora por trás de uma antiga lenda chinesa em Immortal Legacy</p>
                                    <a href="https://www.playstation.com/pt-br/games/immortal-legacy-the-jade-cipher-ps4/">Saiba Mais</a>
                                </div>
                            </div>
                           
                            </div>
                        </div>
                </aside>
            </div>
            <hr class="mt-4 mb-4">
        </article>        
        <section class="container pt-3 pb-3 pl-0 pr-0" style="background:">
            <div class="row p-0 m-0" style="background:">
                <div class="col-lg-6 pl-1 pb-0 pt-2">
                    <h3><span class="place"></span> Headsets estão cada vez mais acessíveis</h3>
                    <div class="m-0 mb-3 p-0">
                        <p style="margin-bottom: -15px; padding-bottom: 0px;"> Várias empresas já lançaram headsets de realidade virtual, numa tentativa de atrair os jogadores para essa forma de jogar relativamente nova. As opções são várias em termos de qualidade e preço,</p><p> como seria de esperar, por ser um item tecnológico que ainda busca sua posição ideal.</p>
                        <p>No entanto, a situação já não é a mesma dos anos anteriores, pois os headsets de RV estão ficando acessíveis e vêm apresentando um alto desempenho. </p>
                        <p> Você pode comprar um modelo de headset para o seu smartphone por poucos dólares, embora os preços dos modelos de ponta ainda estejam perto dos milhares.</p>
                        <p> HP, Dell, Acer, Samsung, Lenovo e Sony são apenas algumas das principais empresas de tecnologia que lançaram seus próprios headsets de RV, mas há várias outras ainda em fase de desenvolvimento.</p>
                        <p> Além das marcas listadas acima, o headset mais famoso hoje no mercado talvez seja o Oculus Rift, que estabeleceu os padrões a serem seguidos pelos demais provedores de RV. </p>
                        
                    </div>
                </div>
                <div class="col-lg-6 blockimg allimg p-0" style="">
                    <img src="images/sthefany/oculos.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                </div>
            </div>
            <div class="col-12 mt-5 mb-5">
                <div class="row">
                    <div class="col-md-4">
                        <h3><span class="place"></span> Mas cadê o conteúdo?</h3>
                        <div class="row align-items-center m-0 mt-4 mb-4">
                            <img src="images/sthefany/jogo1.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:110%;" />
                        </div>
                        <p>Já existem muitos jogos ótimos de RV disponíveis no mercado, embora a qualidade deles seja, obviamente, variável, assim como a dos jogos normais de videogame. Alguns dos melhores jogos de VR (Virtual Reality) incluem o excelente Star Trek: Bridge Crew, em que você e seus parceiros podem assumir o controle de uma nave danificada.
                        O jogo Doom em RV combina a solução de problemas com ação para valer numa versão de tirar o fôlego desse FPS (first-person shooter) clássico.
                        Finalmente, o “The Lab” proporciona um primeiro contato perfeito com os jogos de RV, já que você pode jogar vários minijogos, incluindo jogos de arco e flecha e estilingue com os quais você pode testar o seu equipamento de RV.</p>
                    </div>
                    <div class="col-md-4">
                        <h3><span class="place"></span> Aplicação em outras áreas</h3>
                        <div class="mt-4 mb-4">
                            <img src="images/sthefany/level.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                        </div>
                        <p>A realidade virtual não é apenas para jogos de videogame. Também está se tornando mais comum no setor dos cassinos online.

                        Alguns já abraçaram o mundo da RV, como os saguões de cassino online que podem ser visitados com um dispositivo Oculus.

                        Em alguns sites, também é possível circular no interior de caça-níqueis e ver a cascata de símbolos rodando à sua volta. O pôquer é outro jogo de cassino com uma série de aplicações potenciais para a RV. Você já pode entrar em salas de pôquer e jogar como um avatar, embora os gráficos ainda sejam bastante básicos.</p>
                    </div>
                    <div class="col-md-4">
                        <h3><span class="place"></span> E o futuro?</h3>
                        <div class="mt-4 mb-4">
                            <img src="images/sthefany/future.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                        </div>
                        <p>A última geração de dispositivos de realidade virtual só surgiu realmente em 2010. Com um período de vida tão curto, ainda há muito espaço para que estes dispositivos se desenvolvam ainda mais. O que antes parecia ficção científica, agora está se tornando rapidamente fato científico.

                        Podemos vislumbrar jogadores entrando em espaços virtuais muito semelhantes aos reais sem saírem de suas casas. Em vez de trocarmos mensagens nas redes sociais ou de nos encontrarmos para um café, talvez, no futuro, socializaremos através da realidade virtual.

                        Imagine que esteja embarcando com amigos para viver aventuras selvagens em universos inimagináveis, tudo no conforto da sua sala de estar.

                        O que quer que seja que o futuro reserve para os jogos de realidade virtual, podemos ter a certeza de que a viagem será emocionante.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>