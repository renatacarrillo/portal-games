<!-- TRADUÇÃO da PÁGINA -->
<?php
include('includes/process.php');

if (isset($_POST['PT'])) {
    $lang = $_POST['PT'];
    $aLang = Translate($lang);
} elseif (isset($_POST['EN'])) {
    $lang = $_POST['EN'];
    $aLang = Translate($lang);
} else {
    $aLang = $_SESSION['LANG'];
}
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 03 do tema de 'Desenvolvedoras'">
    <meta name="author" content="FERNANDO ZARA | @contato: fzc92@hotmail.com">
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>
    <div class="devcreators">
        <header>
            <div>
                <?php
                // incluindo topo da página 
                include('includes/header.php');

                echo '<br>';
                // breadcrumb
                breadcrumb(array('index.php' => 'Home', '' => 'Desenvolvedoras de Games'));

                include('includes/lang.php');
                ?>
            </div>
        </header>
        <article>
            <div class="container">
                <hr class="hr">

                <div class="row">
                    <div class="col-12 mt-4 mb-4">
                        <h1 class="text-center"><?php echo $aLang['114']; ?></h1>
                        <p class="lead text-center"><?php echo $aLang['116']; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <img class="mx-auto d-block img-fluid" src="./images/Zara/Como-se-tornar-um-desenvolvedor-de-jogos.webp" alt="Logo desenvolvedores de jogos">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <img class="mx-auto d-block img-fluid" src="./images/Zara/jogosdigitais.webp" alt="">
                    </div>
                    <div class="col-md-6 mt-4">
                        <p class="text">Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss,
                            que um alcoolatra
                            anonimis. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae
                            iaculis
                            nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.
                            Viva
                            Forevis aptent taciti sociosqu ad litora torquent.</p>
                        <p class="text">Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss,
                            que um alcoolatra
                            anonimis. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae
                            iaculis
                            nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.
                            Viva
                            Forevis aptent taciti sociosqu ad litora torquent.</p>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-md-6 mt-4">
                        <img class="mx-auto d-block img-fluid" src="./images/Zara/game_engine.webp" alt="">
                    </div>
                    <div class="col-md-6 mt-4">
                        <p class="text">Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss,
                            que um alcoolatra
                            anonimis. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae
                            iaculis
                            nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.
                            Viva
                            Forevis aptent taciti sociosqu ad litora torquent.</p>
                        <p class="text">Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss,
                            que um alcoolatra
                            anonimis. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae
                            iaculis
                            nisl. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.
                            Viva
                            Forevis aptent taciti sociosqu ad litora torquent.</p>
                    </div>
                </div>
            </div>

        </article>
        <section class="container mobileTheme">
            <div class="row mt-5">
                <div class="col">
                    <h3 class="text-center mt-2"><?php echo $aLang['102']; ?></h3>
                    <hr class="hr">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-5 text-center">
                    <a href="devfamosos.php">
                        <h4><?php echo $aLang['113']; ?></h4>
                    </a>
                    <a href="devfamosos.php"><img src="./images/Zara/hideo_kojima.webp" alt="hideo kojima desenvolvedor de jogos" width="200" height="200"></a>
                </div>
                <div class="col-md-6 mb-5 text-center">
                    <a href="devcriators.php">
                        <h4><?php echo $aLang['6']; ?></h4>
                    </a>
                    <a href="devcriators.php"><img class="imagem2" src="images/Zara/Desenvolvedor-de-Games.webp" alt="" width="200" height="200"></a>
                </div>
            </div>

        </section>
    </div>
    </div>
    <!--Footer-->
    <?php
    // incluindo footer da página
    include('includes/footer.php');
    ?>
</body>

</html>