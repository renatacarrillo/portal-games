<?php  
 // TRADUZ A PÁGINA 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="gamestuff">
    <link rel="icon" href="images/favicon3.jpg">
    <title>Gamestuff &#8226; Era 80/90</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mine.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


</head>
<div class="color-background-gradient">

    <body class="color-background-gradient">

        <header>
        <?php
            include('includes/header.php');
        ?>
            <div class="videogames">
                <?php                    
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php'=>'Home', 'SITE - V 1.9/bestxxihome.php'=>'Video Games Home', '' => 'Era 80 e 90'));
					
					include('includes/lang.php'); 
                ?>
            </div>

        </header>

  
		<div class="container"><hr class="hr"></div>
        
        <div class="jumbotron ">
            <div class="container">        
                <h1 class="jumbotron-heading">Lorem</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                <img src="images/Anderson/atari.webp" alt="foto primeiro video-game da histĂ³ria" width="auto" height="auto">
                <p>
                    <a href="https://pt.wikipedia.org/wiki/Atari"  class="btn btnanderson btn-primary my-2">Read</a>
                </p>
            </div>
        </div>
      <div class="album ">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="card 1 ">
                <img class="card-img-top" src="images/Anderson/master.webp" alt="Foto criador magnavox odssey" width="100" height="200">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                  <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                      <a href="era2000.php"  class="btn btnanderson btn-primary my-2">Era 2000</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card 2">
                <img class="card-img-top" src="images/Anderson/nintendo.webp" alt="Foto jogo pong" width="100" height="200">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                    <a href=https://pt.wikipedia.org/wiki/Nintendo#Color_TV_Game  class="btn btnanderson btn-primary my-2">Read</a>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card 3">
               <img class="card-img-top" src="images/Anderson/games%20nostalgicos.webp" alt="Foto jogo pong" width="100" height="200">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                    <a href="https://www.youtube.com/watch?v=jhLb20XgwUE"  class="btn btnanderson btn-primary my-2">Link</a>
              
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            
        <footer>
            <?php  
                include('includes/footer.php'); 
            ?>
        </footer>
    </body>
	</div>
</div>

</html>