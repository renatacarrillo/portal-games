<!-- TRADUÇÃO da PÁGINA -->
<?php
include('includes/process.php');

if (isset($_POST['PT'])) {
    $lang = $_POST['PT'];
    $aLang = Translate($lang);
} elseif (isset($_POST['EN'])) {
    $lang = $_POST['EN'];
    $aLang = Translate($lang);
} else {
    $aLang = $_SESSION['LANG'];
}
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 01 do tema de 'Mobile e Portáteis'">
    <meta name="author" content="LUCAS JUSTI | @contato: lucas.silva563@fatec.sp.gov.br">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Lançamentos Mobile</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>
    <div class="mobilereleases">
        <header>
            <div>
                <?php
                // incluindo topo da página 
                include('includes/header.php');

                echo '<br>';
                // breadcrumb
                breadcrumb(array('index.php' => 'Home', '' => 'Lançamentos Mobile'));

                include('includes/lang.php');
                ?>
            </div>
        </header>
        <main class="container">
            <hr class="hr">
            <article>
                <div class="row">
                    <div class="col-12 mt-5 mb-4">
                        <h1 class="text-center"><?php echo $aLang['100']; ?></h1>
                        <p class="lead text-center"><?php echo $aLang['101']; ?></p>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Mario Kart Tour</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/m_eIGTQgK4s" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row invert">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Harry Potter: Wizards Unite</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3W0rhvv5DM4" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Gears Pop!</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3Pf4KxbknXA" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row invert">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Minecraft Earth</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YQx32XUjZF8" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Shadowgun War Games</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tHmdbb1s1VU" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row invert">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Human: Fall Flat</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-Edk59BqSEU" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Forza Street</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/07TffPtB_Jg" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row invert">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Call of Duty: Mobile</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/b4WD8emZwPs" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">Bad North</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/WMMBZxgub_U" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row invert">
                        <div class="col-12 col-md-6 mt-4">
                            <h2 class="text-left">H1Z1</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                        </div>
                        <div class="col col-md-6 mt-4">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/_1SSMKlpgL8" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                </div>
            </article>
            <section class="container mobileTheme">
                <div class="row mt-5">
                    <div class="col">
                        <h3 class="text-center mt-2"><?php echo $aLang['102']; ?></h3>
                        <hr class="hr">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-5 text-center">
                        <a href="mobileranking.php">
                            <h4><?php echo $aLang['103']; ?></h4>
                        </a>
                        <a href="mobileranking.php"><img src="images/Lucas/ranking.webp" alt="numeros dispostos em forma de escada" width="200" height="200"></a>
                    </div>
                    <div class="col-md-6 mb-5 text-center">
                        <a href="mobileevolution.php">
                            <h4><?php echo $aLang['104']; ?></h4>
                        </a>
                        <a href="mobileevolution.php"><img src="images/Lucas/handheldlink.webp" alt="video-games portáteis sobre uma mesa" width="200" height="200"></a>
                    </div>
                </div>
            </section>
        </main>
        <!--Footer-->
        <?php
        // incluindo footer da página
        include('includes/footer.php');
        ?>
    </div>
</body>

</html>