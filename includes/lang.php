<?php

if (session_status() != 2) {
    session_start();
}

?>
 
  <div class="text-center gameblock">
    <h2 key="jogos">GAMESTUFF</h2>
    <!-- ESCOLHE IDIOMA -->
        <form method="GET" action="includes/createsession.php">
            <div class="lang">
                <span>Idioma / Language:</span>
                <button type="submit" value="pt-BR" id="PT" name="lang"><img src='images/brazil.png' alt=''/></button>
                <button type="submit" value="en" id="EN" name="lang"><img src='images/usa.png' alt=''/></button>
            </div>
        </form>
    </div>