<?php
    //Autor: Vinícius Lessa - 05/06/2019
    //Obs.: Essa função será chamada em toda página do portal, trazendo como parâmetro o Idioma escolhido
    //pelo usuários. Esse valor será capiturado através do Método 'POST' na própria página.
    function Translate($message) {
        //Se PORTUGUÊS (Obs.: NÃO É DEFAULT, pois deverá ser setado no BUTTON de seleção de idioma)
        $messages = array(
            'GAMES MARCANTES DESSE SÉCULO' => 'BEST GAMES OF THE CENTURY',
            'CLÁSSICOS'                    => 'Classic Games',
            'PC'                           => 'PC',
            'SIMULADORES'                  => 'Simulators',
            'LANÇAMENTOS'                  => 'Releases',
            'DESENVOLVEDORAS'              => 'Developers',
            'OUTROS'           => 'OTHER',
            'CONTATOS'           => 'CONTACT US',
            'QUEM SOMOS'           => 'ABOUT US',
            'CRIAR CONTA'           => 'CREATE ACCOUNT',
            'MELHORES DO SÉCULO'           => 'BEST OF THE CENTURY',
            'ESTRUTURA ORIGIAL DO SITE'           => 'WEBSITE ORIGINAL STRUCTURE',
            'ÚLTIMAS NOVIDADES'           => 'LATEST NEWS',
            'OS JOGOS QUE MARCARAM ÉPOCA'           => 'MOST IMPORTANT GAMES',
            'ARTIGOS RELACIONADOS'           => 'RELATED ARTICLES',
            'TOP 10 MOBILE'           => 'TOP 10 MOBILE',
            'HISTÓRIA DOS PORTÁTEIS'           => 'HANDHELD CONSOLES HISTORY',
            'OS MAIS JOGADOS'           => 'THE MOST PLAYED',
            'Os jogos mais populares para mobile do mundo.'           => 'Most popular mobile games in the world',
            'A EVOLUÇÃO DOS PORTÁTEIS'           => 'HANDHELD CONSOLES EVOLUTION',
            'Os games mais esperados para mobile - ação, corrida, estratégia e mais!'           => 'The most ancticipated mobile games - action, racing, strategy and more!',
            'O avanço tecnológico dos videogames portáteis ao longo das ultimas décadas.'           => 'The tecnological advance of handheld consoles in the last decades',
            'FALE CONOSCO'           => 'LEAVE A MESSAGE',
            'TELEFONE'           => 'PHONE',
            'DESENVOLVEDORES FAMOSOS'           => 'FAMOUS DEVELOPERS',
            'COMO SER UM DESENVOLVEDOR'           => 'HOW TO BECOME A DEVELOPER',
            'DESENVOLVEDORES DE GAMES'           => 'GAME DEVELOPERS',
            'APRENDA A SER UM DESENVOLVEDOR DE GAMES'           => 'LEARN HOW TO BE A GAME DEVELOPER',
            'Jogos do Século XXI'           => 'Games of the XXI century',
            'Ano Vencedor:'           => 'Year awarded:',
            'Prêmio - Jogos do Ano'           => 'Awards - Game of the Year',
            'Vencedores do prêmio de JOGO DO ANO'           => 'GAME OF THE YEAR WINNERS',
            'Fonte'           => 'Source',
            'O que é o Prêmio de Jogo do Ano?'           => 'What is the Game of the Year Awards?',
            'Melhores Games do Século XXI'           => 'Best games of the XXI century',
            'Hitória dos jogos para pc'           => 'PC games history',
            'Tipos de computadores'           => 'Types of computers',
            'Diferenças entre pc gamer e videogames.'           => 'Difference between gamer PCs and consoles',
            'Hardware, especificações e inovações'           => 'Hardware, specifications and innovation.',
            'Diferença entre PC GAMER de alto custo e um de baixo custo'           => 'Difference between high end gamer PC and low budget.',
            'Momento de diversão, jogue o clássico PRINCE OF PERSIA!'           => 'Time for fun, play the classic PRINCE OF PERSIA.',
            'Momento de diversão, jogue o clássico DOOM!'           => 'Time for fun, play the classic DOOM',
            'Momento de diversão, jogue o clássico RAPTOR!'           => 'Time for fun, play the classic RAPTOR',
            'Conheça a história dos jogos para PC e suas evoluções.'           => 'Learn about the PC games history and their evolution.',
            'Conheça o universo dos jogos On-line para PC.'           => 'Learn about the On-line PC gaming.',
            'Jogos exclusivos para pc.'           => 'PC exclusive games.',
            'Melhores jogos para Pc da Atualidade:'           => 'Best games for PC nowadays',
            'Conheça o universo dos jogos On-line para PC.'           => 'Know more about the On-line games universe.',
            'Tudo o que rola no fantástico mundo dos PC Gamers.'           => 'Everithing thats going on in the fantastic world of PC gamers.',
             
        );
        if ($_SESSION['lang'] == 'pt-BR') {
          return $message;
        }

        return $messages[$message];

        if(true){
            $aLang = array(
              //NAV
                '0' => "Jogos", 
                //'1' => "Video-Games",
                //'2' => "Mobile",
                '1' => "Clássicos",
                '2' => "PC",
                '3' => "Simuladores",
                '4' => "Lançamentos",
                '5' => "Melhores do Século",
                '6' => "Desenvolvedoras",
                '7' => "Outros",
                '8' => "Contatos",
                '9' => "Quem Somos",
                '10' => "Normas de utilização",
                '11' => "Cadastro",
                '12' => "Estrutura do Site Original",
                '13' => "Criar Conta",
              //HOME
                '14' => "Últimas Novidades",
                '15' => "GAMES MARCATES DESSE SÉCULO",
              //MELHORES DO SÉCULO 01
                '20' => "Os Jogos que Marcaram época.",
                '21' => "O que há para ver?",
                '22' => "Últimas Novidades",
                '23' => "Últimas Novidades",
                '24' => "Últimas Novidades",
              //MELHORES DO SÉCULO 02
              //MELHORES DO SÉCULO 03
              //MOBILE
                '100' => "LANÇAMENTOS",
                '101' => "Os games mais esperados para mobile - ação, corrida, estratégia e mais!",
                '102' => "ARTIGOS RELACIONADOS",
                '103' => "TOP 10 MOBILE",
                '104' => "HISTÓRIA DOS PORTÁTEIS",
                '105' => "OS MAIS JOGADOS",
                '106' => "Os jogos mais populares para mobile do mundo.",
                '107' => "A EVOLUÇÃO DOS PORTÁTEIS",
                '108' => "O avanço tecnológico dos videogames portáteis ao longo das ultimas décadas.",
              //CONTATOS
                '120' => "FALE CONOSCO",
                '121' => "CONTATOS",
                '122' => "TELEFONE",
                '123' => "QUEM SOMOS",
                //Desenvolvedoras
                '113' => "DESENVOLVEDORES FAMOSOS",
                '114' => "COMO SER UM DESENVOLVEDOR",
                '115' => "DESENVOLVEDORES DE GAMES",
                '116' => "APRENDA A SER UM DESENVOLVEDOR DE GAMES"

            );
          //Se INGLÊS 
        } elseif ($lang == 'EN') {
            $aLang = array(
              //HOME 
                '0' => "Games", 
                //'1' => "Video-Games",
                //'2' => "Mobile",
                '1' => "Classic Games",
                '2' => "Computer Games",
                '3' => "Simulators",
                '4' => "New Games",
                '5' => "Best of the Century",
                '6' => "Developers",
                '7' => "Other",
                '8' => "Contact",
                '9' => "About Us",
                '10' => "Terms of Use",
                '11' => "Sign Up",
                '12' => "Our Original Site",
                '13' => "Create Account",
              //HOME
                '14' => "Last News",
                '15' => "MARCHING GAMES OF THIS CENTURY",
              //MELHORES DO SÉCULO 01
                '20' => "'The games that marked the season.'",
                '21' => "What's the content?",
                '22' => "Últimas Novidades",
                '23' => "Últimas Novidades",
                '24' => "Últimas Novidades",
              //MELHORES DO SÉCULO 02
              //MELHORES DO SÉCULO 03       
              //MOBILE
                '100' => "RELEASES",
                '101' => "The most anticipated mobile games - action, racing, strategy and more!",
                '102' => "RELATED ARTICLES",
                '103' => "TOP 10 MOBILE",
                '104' => "HANDHELD CONSOLES HISTORY",
                '105' => "THE MOST PLAYED",
                '106' => "The most popular mobile games in the world.",
                '107' => "THE EVOLUTION OF HANDHELD CONSOLES",
                '108' => "The tecnological advance of handheld consoles in the last decades.",
                //CONTATOS
                '120' => "LEAVE A MESSAGE",
                '121' => "CONTACT US",
                '122' => "PHONE",
                '123' => "ABOUT US",
                //Desenvolvedoras
                '113' => "FAMOUS DEVELOPERS",
                '114' => "HOW TO BECOME A DEVELOPER",
                '115' => "GAME DEVELOPERS",
                '116' => "LEARN HOW TO BE A GAME DEV"

                );
            }
            //Retordo do ARRAY selecionado para ser usado em cada página
            return $aLang;
        }

    //Sempre no primeiro acesso as páginas, nenhuma opção foi 'Setada'. Por isso, atribui uma variável
    //SUPER GLOBAL que nada mais é que o array montado acima em Português, ou seja, sempre as páginas
    //serão carregas em PORTUGUÊS
    session_start();

    $aLang = array(
        '0' => "Jogos", 
        //'1' => "Video-Games",
        //'2' => "Mobile",
        '1' => "Clássicos",
        '2' => "PC",
        '3' => "Simuladores",
        '4' => "Lançamentos",
        '5' => "Melhores do Século",
        '6' => "Desenvolvedoras",
        '7' => "Outros",
        '8' => "Contatos",
        '9' => "Quem Somos",
        '10' => "Normas de utilização",
        '11' => "Cadastro",
        '12' => "Estrutura do Site Original",
        '13' => "Criar Conta",
        //HOME
        '14' => "Últimas Novidades",
        '15' => "GAMES MARCATES DESSE SÉCULO",
      //MELHORES DO SÉCULO 01
        '20' => "'Os Jogos que Marcaram época.'",
        '21' => "O que há para ver?",
        '22' => "Últimas Novidades",
        '23' => "Últimas Novidades",
        '24' => "Últimas Novidades",
      //MELHORES DO SÉCULO 02
      //MELHORES DO SÉCULO 03
      //MOBILE
        '100' => "LANÇAMENTOS",
        '101' => "Os games mais esperados para mobile - ação, corrida, estratégia e mais!",
        '102' => "ARTIGOS RELACIONADOS",
        '103' => "TOP 10 MOBILE",
        '104' => "HISTÓRIA DOS PORTÁTEIS",
        '105' => "OS MAIS JOGADOS",
        '106' => "Os jogos mais populares para mobile do mundo.",
        '107' => "A EVOLUÇÃO DOS PORTÁTEIS",
        '108' => "O avanço tecnológico dos videogames portáteis ao longo das ultimas décadas.",
        //CONTATOS
        '120' => "FALE CONOSCO",
        '121' => "CONTATOS",
        '122' => "TELEFONE",
        '123' => "QUEM SOMOS",

        //Desenvolvedoras
        '113' => "DESENVOLVEDORES FAMOSOS",
        '114' => "COMO SER UM DESENVOLVEDOR",
        '115' => "DESENVOLVEDORES DE GAMES",
        '116' => "APRENDA A SER UM DESENVOLVEDOR DE GAMES"
        
    );

    $_SESSION['LANG'] = $aLang;
?>
