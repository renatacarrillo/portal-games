<?php
    echo "<div class='container-fluid'>
            <div class='row'>
                <footer class='col cornavfooter'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col-sm-4'>
                                <h5>Painel</h5>
                                <ul>
                                    <li><a href='#'>Página Inicial</a></li>
                                    <li><a href='#'>Login</a></li>
                                    <li><a href='#'>Quiz</a></li>
                                </ul>
                            </div>
                            <div class='col-sm-4'>
                                <h5>Sobre nós</h5>
                                <ul>
                                    <li><a href='quemsomos.php'>Company Information</a></li>
                                    <li><a href='contato.php'>Contact us</a></li>
                                    <li><a href='#'>Reviews</a></li>
                                </ul>
                            </div>
                            <div class='col-sm-4'>
                                <h5>Legal</h5>
                                <ul>
                                    <li><a href='#'>Terms of Service</a></li>
                                    <li><a href='#'>Terms of Use</a></li>
                                    <li><a href='#'>Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class='social-networks'>
                        <a href='https://twitter.com' class='twitter' target='_blank'><i class='fa fa-twitter'></i></a>
                        <a href='https://www.facebook.com' class='facebook' target='_blank'><i class='fa fa-facebook-official'></i></a>
                        <a href='https://accounts.google.com' class='google' target='_blank'><i class='fa fa-google-plus'></i></a>
                    </div>
                    <!--<p class='float-right'>
                        <a href='#'>Back to top</a>
                    </p> -->
                    <div>
                        <p>&copy; 2019 Copyright Gamestuff</p>
                    </div>
                </footer>
            </div>
        </div>";
?>