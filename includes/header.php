<html>
<?php 
    header("Content-type: text/html; charset=UTF-8");

	echo "<nav class='navbar navbar-expand-md navbar-dark cornavfooter fixed-top'>
            <a class='navbar-brand' href='index.php'>Gamestuff</a> <!-- home -->
			<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExampleDefault' aria-controls='navbarsExampleDefault' aria-expanded='false' aria-label='Toggle navigation'>
				<span class='navbar-toggler-icon'></span>
			</button>
			<div class='collapse navbar-collapse' id='navbarsExampleDefault'>
				<ul class='navbar-nav mr-auto'>
                <li class='nav-item dropdown'>
						<a class='nav-link dropdown-toggle' href='#' id='dropdown01' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>". $aLang[0]."</a>
						<div class='dropdown-menu' aria-labelledby='dropdown01' style='border:0 2 2 2; box-shadow:0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius:0px;'>
                            <a class='dropdown-item' href='videogameshome.php'>Videogames</a>
                            <a class='dropdown-item' href='mobilereleases.php'>Mobile</a>
                            <a class='dropdown-item' href='classicoshome.php'>". Translate('Clássicos') ."</a>
							<a class='dropdown-item' href='pchome.php'>". $aLang[2]."</a>
							<a class='dropdown-item' href='vrgameshome.php'>". $aLang[3]."</a> 
						</div>
					</li>
				
					<li class='nav-item'>
						<a class='nav-link' href='lancamentoshome.php'>". $aLang[4]."</a>
						<!---->
					</li>
				
					<li class='nav-item'>
						<a class='nav-link' href='bestxxihome.php'>". $aLang[5]."</a>
						<!---->
					</li>
				
					<li class='nav-item'>
						<a class='nav-link' href='devcriators.php'>". $aLang[6]."</a>
						<!---->
					</li>
				
					<li class='nav-item dropdown'>
						<a class='nav-link dropdown-toggle' href='#' id='dropdown01' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>". $aLang[7]."</a>
						<div class='dropdown-menu' aria-labelledby='dropdown01' style='border:0 2 2 2; box-shadow:0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius:0px;'>
							<a class='dropdown-item' href='contato.php'>". $aLang[8]."</a>
							<a class='dropdown-item' href='quemsomos.php'>". $aLang[9]."</a>
							<a class='dropdown-item' href='politicasdeuso.php'>". $aLang[10]."</a>
                            <a class='dropdown-item' href='link5.html'>". $aLang[11]."</a>
                            <a class='dropdown-item' href='link5.html'>". $aLang[12]."</a>
                            <a class='dropdown-item' href='criarconta.php'>". $aLang[13]."</a>
						</div>
					</li>
                </ul>

                

                <!-- <a href='index.php' style='margin-right: 10px;'> <img src='./images/usa.png' alt=''/></a>
                <a href='index.php'> <img src='./images/brazil.png' alt=''/></a> -->

				<!-- botao de pesquisa
			
			<form class='form-inline my-2 my-lg-0'>
				<input class='form-control mr-sm-0' type='text' placeholder='Search' aria-label='Search'>
				<button class='btn btn-secondary my-2 my-sm-0' type='submit'>Search</button>
			</form> -->
			</div> </nav>";

        /*-- CONDICIONAL PARA MONTAGEM DO BREADCRUMB --*/
        
        function breadcrumb($url_pieces = array(), $divisor = '>') {        
         //verifica se foram passados parametros
         if ($url_pieces) {
            $url_crumb = $url_pieces;
            $http = null;
         } else {
            //senão não houver parametro
            //então criar a url automaticamente
            $http = 'http://'; 
            $request = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
            $explode = explode('/', $request);
            foreach($explode as $explode) {
                $url_crumb[str_replace('.php', '', $explode)] = str_replace('.php', '', $explode);
            }
         }
         //quantidade de fragmentos da url
            $count = sizeof($url_crumb);
         //inicia contador
            $i = 1;
            foreach($url_crumb as $link=>$value) {
                //verifica se é o primeiro fragmento da url
                if($i == 1) {
                    $href = $http.$link;
                } else {
                    $href = '/'.$link;
                }
                //verifica se é o ultimo fragmento da url
                if($i == $count) {
                    //mostrar fragmento sem link
                    $crumb[] = '<span>'.($value).'</span>';
                } else {
                    //mostrar fragmento com link para a pagina
                    $crumb[] = '<a href="'.$href.'" title="'.$value.'">'.$value.'</a>&nbsp'.$divisor.'&nbsp';
                }
                $i++;
            }
         //mostrar breadcrumb na tela
            echo "<div class='breadcrumb'>";
            foreach($crumb as $crumb) {
                echo $crumb;
            }
            echo "</div>";
        }
?>
</html>