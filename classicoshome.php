<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 01 do tema de 'Jóogos Marcantes do Século XXI', contendo informações sobre os games que mais marcaram desde o ano 2000!">
    <meta name="author" content="VINÍCIUS LESSA | @contato: vinícius.lessa33@outlook.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>
    <div class="classicos">
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php' => 'Home', '' =>'Melhores Games desse Século'));

                    include('includes/lang.php');  
            ?>
            </div>
        </header>

        <article class="container">
            <hr class="hr">
            <div class="row mt-5">
                <div class="col-lg-8">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="p-0 m-1 mb-3">
                                    <div>
                                        <h1 class="mb-1">GAMES MARCANTES DESSE SÉCULO</h1>
                                        <h3 class="mb-4">TOP FIVE: Os games que marcaram época</h3>
                                        <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.</p>
                                        <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                        <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>

                                    </div>
                                    <hr class="mt-4 mb-4">
                                    <div class="row p-0 m-0" style="background:;">
                                        <div class="col-lg-12 pl-1 pb-0 pt-0">
                                            <h3><span class="place">1º</span> RED DEAD REDEMPTION 2</h3>
                                            <h5>O novo game da Rockstar é a continuação de um dos games mais aclamados
                                                de todos os tempos:</h5>
                                        </div>
                                        <div class="col-lg-6 pl-1 pb-0 pt-0">
                                            <div class="m-0 mb-3 p-0">
                                                <p style="background: ; margin-bottom: -15px; padding-bottom: 0px;">
                                                    Red Dead Redemption 2, disponível para PS4 e Xbox One, é a
                                                    continuação de um dos games mais aclamados
                                                    de todos os tempos. O título da Rockstar (produtora de GTA 5 e
                                                    Bully) coloca os jogadores na pele de Arthur Morgan, um fora da lei
                                                    que precisa se reerguer junto de sua gangue e sobreviver no coração
                                                    dos Velho Oeste americano
                                                </p>
                                                <a href="#" style="background:;"><br>Saiba Mais</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 blockimg allimg p-0" style="">
                                            <img src="images/reddead1.jpg" class="rounded h-100" alt="teste"
                                                style="max-width: 100%; max-heght:100%;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <aside class="col-lg-4 p-0 mt-1" style="height: auto;">
                    <div class="col p-0 asideblock">
                        <div class="p-3" style="background: rgba(0,0,0,0.7);">
                            <h3 class="text-center pb-3">Game of the Year</h3>
                            <div class="row pl-2 pr-2 mt-2 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>GTA V</h4>
                                    <h5>Ano Vencedor: 2018</h5>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis Interagi.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                            <div class="row pl-2 pr-2 mt-3 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>GTA V</h4>
                                    <h5>Ano Vencedor: 2018</h5>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis Interagi.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                            <div class="row pl-2 pr-2 mt-3 mb-3">
                                <div class="col-5 imgaside allimg">
                                </div>
                                <div class="col-7">
                                    <h4>GTA V</h4>
                                    <h5>Ano Vencedor: 2018</h5>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis Interagi.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                            <div class="row pl-2 pr-2 mt-3 mb-1">
                                <div class="col-12 mt-4">
                                    <h3 class="text-center">Game of the Year Award</h3>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis Interagi. Mussum Ipsum, cacilds vidis
                                        litro abertis Interagi.
                                        Mussum Ipsum, cacilds vidis litro abertis Interagi.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                        </div>
                </aside>
            </div>
            <hr class="mt-4 mb-4">
        </article>        
        <section class="container pt-3 pb-3 pl-0 pr-0" style="background:">
            <div class="row p-0 m-0" style="background:">
                <div class="col-lg-6 pl-1 pb-0 pt-2">
                    <h3><span class="place">2º</span> THE ELDER SCROLLS V:</h3>
                    <div class="m-0 mb-3 p-0">
                        <p style="margin-bottom: -15px; padding-bottom: 0px;">Se existe uma frase que possa definir o jogo The Elder Scrolls V: Skyrim, essa frase é: “Diga adeus à sua vida real”, e não há uma definição melhor para ele, uma vez que você mergulha na história e começa a se aventurar em mais um pedaço do reino de Tamriel.
                        Trata-se de uma aventura contínua, um mundo vasto e que transmite uma coisa não muito comum em NPCs (personagens não jogáveis) do cenário: Vida. As pessoas nas cidades trabalhando, e indo para suas casas para uma boa noite de sono, uma rotina que se repete dia após dia dentro do jogo, e uma grande interação com o jogador.
                        </p>
                        <a href="#" style="background:;"><br>Saiba Mais</a>
                    </div>
                </div>
                <div class="col-lg-6 blockimg allimg p-0" style="">
                    <img src="images/skyrim1.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                </div>
            </div>
            <div class="col-12 mt-5 mb-5">
                <div class="row">
                    <div class="col-md-4">
                        <h3><span class="place">3º</span> GTA: SAN ANDREAS</h3>
                        <div class="row align-items-center m-0 mt-4 mb-4">
                            <img src="images/skyrim1.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                        </div>
                        <p>Groove Street, assim como há 10 anos atrás, ainda é o lar do personagem CJ, Carl Johnson,
                            protagonista do game, que retorna a Los Santos (uma versão genérica de Los Angeles) depois
                            de saber da morte de sua mãe. Em contato com os velhos amigos, e seu irmão, Sweet, ele volta
                            a se envolver com o mundo do crime e das batalhas de gangue.</p>
                    </div>
                    <div class="col-md-4">
                        <h3><span class="place">4º</span> THE WITCHER 3</h3>
                        <div class="mt-4 mb-4">
                            <img src="images/witcher.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                        </div>
                        <p>Groove Street, assim como há 10 anos atrás, ainda é o lar do personagem CJ, Carl Johnson,
                            protagonista do game, que retorna a Los Santos (uma versão genérica de Los Angeles) depois
                            de saber da morte de sua mãe. Em contato com os velhos amigos, e seu irmão, Sweet, ele volta
                            a se envolver com o mundo do crime e das batalhas de gangue.</p>
                    </div>
                    <div class="col-md-4">
                        <h3><span class="place">5º</span> GTA: SAN ANDREAS</h3>
                        <div class="mt-4 mb-4">
                            <img src="images/skyrim1.jpg" class="rounded h-100" alt="teste" style="max-width: 100%; max-heght:100%;" />
                        </div>
                        <p>Groove Street, assim como há 10 anos atrás, ainda é o lar do personagem CJ, Carl Johnson,
                            protagonista do game, que retorna a Los Santos (uma versão genérica de Los Angeles) depois
                            de saber da morte de sua mãe. Em contato com os velhos amigos, e seu irmão, Sweet, ele volta
                            a se envolver com o mundo do crime e das batalhas de gangue.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>