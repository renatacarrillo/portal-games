<!-- TRADUZ PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="site sobre jogos">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <meta name="author" content="gamestuff">
    <link rel="icon" href="images/favicon3.png">
    <title>Gamestuff &#8226; home</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mine.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
</head>

<body class="index">
<div class="color-background-gradient">
    <header>
        <div>
            <?php
                // incluindo topo da página
                include('includes/header.php');    
            ?>
        </div>
        <div>
            <div id="imgheader">
               <div class="nilblock">
                </div>
                <div id="linkheader" >
                    <h1 class="display-4" style="color: white;">Home Game Stuff</h1>
                    <h4 class="lead" style="color: gray;">O melhor conteúdo Gamer da WEB</h4><br>
                    <a class="btn btn-outline-secondary" href="quemsomos.php">Saiba mais</a>  
                </div>
                <div class="nilblock">
                </div>
            </div> 
        </div>
        <!-- ESCOLHE O IDIOMA -->
        <?php 
            include('includes/lang.php');  
        ?>
    </header>    
        
    <article class="container" style="background: ;">
    <hr class="hr">
        <div class="row mt-5 ">
            <div class="col-lg-8">
                <div class="container p-0">
                    <div class="row">
                        <div class="col-md-12 p-0" style="background: ;">
                            <div class="p-0 m-1 mb-3">
                                <div class="blockimg allimg reddead">
                                    <img src="images/reddead1.webp" height="230" class="imgarticle" alt="Renata linda"/>
                                </div>
                                <div class="articlediv">
                                    <h2>RED DEAD REDEMPTION 2:</h2>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis, vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de cachacis, eu reclamis.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="p-0 m-1">
                                <div class="blockimg allimg skyrim">
                                    <img src="images/skyrim1.webp" height="230" class="imgarticle" alt="Teste SKYRIM"/>
                                </div>
                                <div class="articlediv">
                                    <h2>THE ELDER SCROLLS V: SKYRIM</h2>
                                    <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis, vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de cachacis, eu reclamis.</p>
                                    <a href="#">Saiba Mais</a>
                                </div>
                            </div>
                        </div>
                        
                        <section class="col-12 mt-3" style="background: ;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row p-3 mt-2 mb-2" style="background:  rgba(0,0,0,0.3);">
                                        <div class="col-5 imgsection allimg">
                                        </div>
                                        <div class="col-7" style="background:">
                                            <h4>Teste Notícia</h4>
                                            <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                            <a href="#">Saiba Mais</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row p-3 mt-2 mb-2" style="background:  rgba(0,0,0,0.3);">
                                        <div class="col-5 imgsection allimg">
                                        </div>
                                        <div class="col-7" style="background:">
                                            <h4>Teste Notícia</h4>
                                            <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                            <a href="#">Saiba Mais</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row p-3 mt-2 mb-2" style="background:  rgba(0,0,0,0.3);">
                                        <div class="col-5 imgsection allimg">
                                        </div>
                                        <div class="col-7" style="background:">
                                            <h4>Teste Notícia</h4>
                                            <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                            <a href="#">Saiba Mais</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row p-3 mt-2 mb-2" style="background:  rgba(0,0,0,0.3);">
                                        <div class="col-5 imgsection allimg">
                                        </div>
                                        <div class="col-7" style="background:">
                                            <h4>Teste Notícia</h4>
                                            <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                            <a href="#">Saiba Mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </section>
                    </div> 
                </div>        
            </div>

            <aside class="col-lg-4 p-0 mt-1" style="height: auto;">
                <div class="col-md p-0 asideblock">
                    <div class="p-3" style="background: rgba(0,0,0,0.3);">
                        <h3 class="text-center pb-3"><?php echo $aLang[14]; ?></h3>
                        <div class="row pl-2 pr-2 mt-2 mb-3">
                            <div class="col-5 imgaside allimg">
                            </div>
                            <div class="col-7" style="background:">
                                <h4>Teste Notícia</h4>
                                <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                <a href="#">Saiba Mais</a>
                            </div>
                        </div>
                        <div class="row pl-2 pr-2 mt-3 mb-3">
                            <div class="col-5 imgaside allimg">
                            </div>
                            <div class="col-7" style="background:">
                                <h4>Teste Notícia</h4>
                                <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                <a href="#">Saiba Mais</a>
                            </div>
                        </div>
                        <div class="row pl-2 pr-2 mt-3 mb-3">
                            <div class="col-5 imgaside allimg">
                            </div>
                            <div class="col-7" style="background:">
                                <h4>Teste Notícia</h4>
                                <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                <a href="#">Saiba Mais</a>
                            </div>
                        </div>
                        <div class="row pl-2 pr-2 mt-3 mb-1">
                            <div class="col-5 imgaside allimg">
                            </div>
                            <div class="col-7">
                                <h4>Teste Notícia</h4>
                                <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis.</p>
                                <a href="#">Saiba Mais</a>
                            </div>
                        </div>
                </div>
        </aside>
        </div>
        <br><br>
    </article>
</div>    
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>    
</body>
</html>