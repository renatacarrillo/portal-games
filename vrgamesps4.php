<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II">
    <meta name="author" content="Sthefany Pereira Nolasco de Souza | @contato: sthefany.nolasco22@gmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body class="vrgames">
    <div>
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';

                    // breadcrumb
                    if (isset($_POST['PT'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simuladores', 'vrgamespc.php' => 'Simuladores PC', ''=> 'Simuladores PS4'));
                    } elseif (isset($_POST['EN'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simulator Games', '' => 'Simulators for PC', '' => 'Simulators PS4'));
                    } else {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simuladores', 'vrgamespc.php' => 'Simuladores PC', ''=> 'Simuladores PS4'));
                    }
                    
                    include('includes/lang.php');  
            ?>
            </div>
        </header>

        <main class="container">
        <hr class="hr">
        <article>
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="text-center mt-5"><?php echo $aLang['25']; ?></h1>
                    <p class="lead text-center mb-4">Os melhores games de realidade virtual para PS4 - ação, corrida, estratégia e mais.</p>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Falcon Age</h2>
                        <p>O que é? Junta-te à heroína Ara e ao seu falcão nesta aventura única na primeira pessoa que te permite procurar recursos, cuidar do teu companheiro alado e treiná-lo para o combate numa tentati</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BHgY8BBoHbs" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Immortal Legacy: The Jade Cipher</h2>
                        <p>O que é? Desvenda a verdade perturbadora por trás de uma antiga lenda chinesa neste jogo de tiros rápido e assustador. Com uma liberdade de movimentos de 360 graus, terás de resolver puzzles ambientais complexos enquanto te livras de todo o tipo de criaturas aterradoras.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OgwPmbKgvWg" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Ace Combat 7: Skies Unknown</h2>
                        <p>O mais recente título deste famoso simulador de combate aéreo coloca-te no cockpit para enfrentares um conjunto de missões e modos de jogo desenvolvidos especificamente para o PlayStation VR. Rasga os céus, sobrevive a combates aéreos mortíferos e sente a emoção da aviação a alta velocidade.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/KQCDj3emnHk" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Ghost Giant</h2>
                        <p>Uma aventura de puzzles encantadora do estúdio responsável por sucessos indie como o Fe e o Stick It To The Man. Entra na pele do gigantesco e fantasmagórico protetor de um rapaz pequeno e solitário chamado Louis. Juntos, enfrentarão todo o tipo de quebra-cabeças e ajudarão os cidadãos de Sancourt.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hEJnJz43e10" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Space Junkies</h2>
                        <p>Um jogo de tiros de realidade virtual direcionado para a experiência multijogador no qual usas um jetpack e combates em arenas espaciais traiçoeiras. Move-te a velocidades estonteantes e tira partido do cenário envolvente para ganhares vantagem sobre os adversários em vários modos de jogo.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/t3xUKjhsjiU" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Table of Tales: The Crooked Crown</h2>
                        <p>Uma aventura mística que se desenrola como um jogo de tabuleiro "vivo". Utiliza o teu PS VR e lidera uma equipa de quatro bandidos numa aventura de espadachim com combate tático por turnos e um elenco de personagens peculiares.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/xqt37kYUPwM" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Jupiter & Mars</h2>
                        <p>Controla dois golfinhos e restaura a paz num reino dos oceanos que tenta recuperar do impacto das alterações climáticas nesta aventura subaquática exclusiva. Utiliza as capacidades únicas de cada criatura para resolver puzzles ambientais e desvendar segredos escondidos pelo mundo do jogo.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/mAH-iOAS2vQ" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">A Fisherman's Tale</h2>
                        <p>ma aventura de puzzles excêntrica onde entras na pele de Bob, um pescador que descobre que não passa de um fantoche de madeira preso dentro de um farol em miniatura, dentro de um farol, dentro de outro farol. Consegues chegar ao topo da torre e escapar a este pesadelo existencial?</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qZtzlnwrggc" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">The Wizards</h2>
                        <p>Um jogo de aventura que te leva numa demanda para salvar o reino de Meliora, uma dimensão paralela repleta de magia. Junta-te a Aurelius, um feiticeiro preso no fluxo temporal, e deixa-te levar no caminho do poder e da glória numa batalha contra forças nefastas que ameaçam as terras. </p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/bELDjkGbvKI" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Dick Wilde 2</h2>
                        <p> Um jogo de tiros colorido no qual sobes para uma jangada e desces um rio a disparar contra todo o tipo de criaturas que te querem para o almoço. O apoio de mira para o comando está incluído para proporcionar um nível adicional de envolvência à ação OTT.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sxuuSCrsgIY" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>          
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>