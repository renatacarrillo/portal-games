<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 01 do tema de 'Lançamentos', contendo informações sobre os mais novos lançamentos do momento">
    <meta name="author" content="GABRIEL SHIRAI MONTEIRO | @contato: gabrielshiraimonteiro@hotmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Lançamentos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>
   
<body>
    <div class="lancamentosgab">
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php' => 'Home', '' =>'Lançamentos'));

                    include('includes/lang.php');   
            ?>
            </div>
        </header>

        <article class="container">
            <hr class="hr">
                <h1 class="mt-3 h1top">LANÇAMENTOS MAIS ESPERADOS</h1>
                <h3 class="mb-1 ml-4">Fique por Dentro de todos os Trailers da E3!</h3>
                <div class="container rounded sombraC" style="background-image: linear-gradient(-90deg, #0d0d0d,#212121, #212121, #0d0d0d);">
                    <div class="row mt-1">
                        <div class="col-lg-8">
                        <iframe class="embed-responsive-item col-lg-8  p-0 mt-3 text-center sombraV" height="560px" style="max-width: 100%" src="https://www.youtube.com/embed/iZh4uQYDlsA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="col-lg-4 text-center mt-1">
                            <div class="col" style="color:white">
                                <iframe class="embed-responsive-item p-0 text-center sombraV" height="190px" style="max-width: 100%" src="https://www.youtube.com/embed/piIgkJWDuQg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe class="embed-responsive-item p-0 text-center sombraV" height="190px" style="max-width: 100%" src="https://www.youtube.com/embed/YApuEWtG30w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe class="embed-responsive-item p-0 text-center sombraV" height="190px" style="max-width: 100%" src="https://www.youtube.com/embed/bH1lHCirCGI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-12 p-0">  
                                <div class=" p-0 m-1 mb-3">
                                    <hr class="hr">
                                    <div class="container-fluid rounded sombraC text-center col-lg-11" style="background-image: linear-gradient(-90deg, #0d0d0d,#212121, #212121, #0d0d0d);">
                                            
                                            <img src="images/Shirai/Price.webp" class="rounded sombraV bordaimg mb-2 text-center mt-2" alt="teste" style="max-width: 100%;" />
                                        </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h3 class="mt-3">A Volta do Capitão!</h3>
                                        <p class="mt-3">Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.</p>
                                        <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi.
                                        </p>
                                                </div>
                                            <div class="col-lg-4 mt-3">
                                                <div class="col">
                                                    <h3 class="h3menor mt-4 bordah3">Páginas Relacionadass</h3>
                                                    <div class="container mt-3">
                                                    <h4><a href="lancamentoshome.php">Conferência Completa da E3</a></h4>
                                                    <h4><a href="https://www.youtube.com/user/gamespot">Sony Traz Novidades</a></h4>
                                                    <h4><a href="https://www.youtube.com/user/gamespot">Mercado de Games Cresce</a></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            </div>    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <section class="container"> 
                <hr class="hr">
                <h3 class="mt-3">Notícias Que Podem lhe Interessar</h3>
                <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 mr-1">
                                        <img src="images/Shirai/vingadores.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%;" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Novos Trailers do game dos Vingadores</a></h3>
                                            <p>A estúdio Square Enix está encarregado de trazer os vingadores ao mundo dos consoles, e ontem eles lançaram alguns trailers que deixou a galera na Conferência da E3 de boaca aberta!... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 ml-1">
                                        <img src="images/Shirai/fifa20.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Fifa 2020 Traz Novidades</a></h3>
                                            <p>Como já era de se esperar, um novo jogo do Fifa foi anunciado nesta E3, porém, o que deixou todos de queixo
                                                caído foi algumas novidades acrescentadas ao game... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 ml-1">
                                        <img src="images/Shirai/borderlands.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Borderlands 3 Anunciado</a></h3>
                                            <p>Em meio a tantos lançamentos esperados para o ano de 2019, tivemos uma bela supresa na E3 realizada no dia 13 de Junho, sexta-feira, que foi o anúncio do terceiro game da franquia Borderlands... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>