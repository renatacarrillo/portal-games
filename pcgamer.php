<?php  
 // TRADUZ A PÁGINA 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<?php
    include('includes/header.php');
?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description"
    content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: 3ª Página do tema de 'Games para PC', informações relacionadas aos games para pc.">

  <meta name="author" content="Marcelo Fernando Silva Barros">
  <link rel="icon" href="images/favicon3.png">
  <title>PC GAMER</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- CSS padrão -->
  <link rel="stylesheet" href="css/style.css">

  <!-- Scripts -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/mine.js"></script>

  <!-- icones footer -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>
<div class="color-background-gradient">

  <body class="color-background-gradient videogames">

    <header>
      <div class="videogames">
        <?php                    
            echo '<br>';
            // breadcrumb
            if (isset($_POST['PT'])) {
              breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/pchome.php' =>'História dos Jogos para PC' , '' =>'Jogos Online para PC', ''=>'PC GAMER'));
            } elseif (isset($_POST['EN'])) {
              breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/pchome.php' =>'PC Games History' , '' =>'Online Games for PC', ''=>'PC GAMER'));
                
            } else {
              breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/pchome.php' =>'História dos Jogos para PC' , '' =>'Jogos Online para PC', ''=>'PC GAMER'));
            }            

            include('includes/lang.php');  
          ?>
      </div>

    </header>
    <section>
      <div class="container">
        <hr class="hr">
        <div class="row">
          <h1 class="text-uppercase">tipos de computadores</h1>
          <p> Os computadores podem ser classificados pelo porte. Existem os de grande porte, mainframes, médio porte,
            minicomputadores e pequeno porte microcomputadores, divididos em duas categorias: os de mesa (desktops) e os
            portáteis (notebooks e handhelds).
            Conceitualmente todos eles realizam funções internas idênticas, mas em escalas diferentes.
            Os Mainframes se destacam por terem alto poder de processamento e muita capacidade de memória, e controlam
            atividades com grande volume de dados, sendo de custo bastante elevado. Operam em MIPS (milhões de
            instruções por segundo).</p>
          <h1 class="text-uppercase">Diferenças entre pc gamer e videogames.</h1>
          <p>No início a principal diferença entre videogames e computadores é que bastava colocar um cartucho em um
            console para jogar, enquanto no PC era necessário fazer instalações. anto um videogame quanto um computador
            rodando um jogo são basicamente constituídos pelos seguintes elementos: processador, memória e placa de
            vídeo. O processador faz os cálculos necessários, executa as funções do jogo, é o cérebro. A memória guarda
            tanto esses dados do processador quanto os gráficos em “estado bruto”, como as texturas dos modelos 3D. A
            placa de vídeo processa esses gráficos e os exibe na tela.
            Em computadores, esses valores estão sempre mudando de usuário para usuário, pois cada um tem sua
            configuração, com potência variável, enquanto nos videogames todos têm o mesmo equipamento. Desta maneira,
            para tentar vender um jogo de PC para o máximo de pessoas possível, os produtores costumam utilizar
            requisitos mínimos baixos.</p>

          <h1 class="text-uppercase"> hardware, especificações e inovações</h1>
          <p> As produtoras indicam quais configurações o seu computador deverá ter para rodar os jogos. Itens como
            memória ram, processador, espaço no HD e um dos principais componentes que é a placa de vídeo. Existem
            modelos de placas de vídeo que chegam a custar mais de R$3.000,00, levando em consideração estes pontos o
            custo-benfício de um PC Gamer não é grande, pois com o passar dos anos ele se tornará obsoleto, comparado ao
            videogame que tem uma vida ativa mais prolongada, pelo menos até o momento.</p>
          <p>Um exemplo de configuração poderosa para o ano de 2019 PC gamer para rodar jogos com ótimo desempenho em
            Full HD ou até em 1440p, o Acer GX-783-BR13 é a melhor escolha. A configuração por aqui é de respeito, com
            um processador Core i7 de 3,4GHz, 16GB de memória RAM, HD híbrido com SSD de 8GB para o sistema operacional
            e uma placa de vídeo dedicada GeForce GTX 1060 de 6GB, um setup de ponta para garantir uma ótima performance
            até mesmo nos games mais pesados, como The Witcher 3, Far Cry 5, Assassin's Creed Origins e outros
            lançamentos do momento. Se você quer uma máquina potente e com alto desempenho, esta é uma ótima opção,
            porém se prepare para gastar ao menos R$5.000,00 em uma máquina com estas especificações</p>

        </div>
      </div>

      <div class="container">
        <hr class="hr">
        <h2 class="text-uppercase text-center">Diferença entre PC GAMER de alto custo e um de baixo custo:</h2>
        <iframe class="col-sm-12" width="560" height="315" src="https://www.youtube.com/embed/v5d95eHjWW8"
          frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>
        <hr class="hr">
      </div>
    </section>

    <div class="album py-3" style="color: white;">
      <div class="container">

        <div class="row">
          <div class="col-md-4">
            <div class="cardmarcelo">
              <img class="card-img-top" src="images/Marcelo/prince.webp" alt="Introdução do jogo Prince of Persia"
                width="100" height="200">
              <div class="card-body">
                <p class="card-text"><b>Momento de diversão, jogue o clássico PRINCE OF PERSIA!</b>
                </p>
                <a href="https://www.retrogames.cz/play_102-DOS.php" target="_blank"
                  class="btn btnmarcelo btn-primary my-2" aria-label="Left Align">Jogue agora!</a>
                <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="cardmarcelo mb-4 box-shadow">
              <img class="card-img-top" src="images/Marcelo/historypc.webp" alt="Computador antigo" width="100"
                height="200">
              <div class="card-body">
                <p class="card-text"><b>Conheça a história dos jogos para PC e suas evoluções.</b>
                </p>
                <a href="pchome.php" class="btn btnmarcelo btn-primary my-2" aria-label="Left Align">Leia mais...</a>
                <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="cardmarcelo mb-4 box-shadow">
              <img class="card-img-top" src="images/Marcelo/online.webp" alt="Jogador profissional de E-sport"
                width="100" height="200">
              <div class="card-body">
                <p class="card-text"><b>Conhecça o universo dos jogos On-line para PC.</b></p>
                <a href="pconline.php" class="btn btnmarcelo btn-primary my-2"> Leia mais...</a>
              </div>

            </div>
          </div>
        </div>
      </div>
      <?php
           include('includes/footer.php'); 
          ?>


  </body>
</div>

</html>