<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 01 do tema de 'Lançamentos', contendo informações sobre os mais novos lançamentos do momento">
    <meta name="author" content="GABRIEL SHIRAI MONTEIRO | @contato: gabrielshiraimonteiro@hotmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Lançamentos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>
   
<body>
    <div class="lancamentosgab">
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php' => 'Home', '' =>'Lançamentos'));

                    include('includes/lang.php');   
            ?>
            </div>
        </header>

        <article class="container">
            <hr class="hr">
                <h1 class="mt-3 h1top">LANÇAMENTOS MAIS ESPERADOS</h1>
                <h3 class="mb-1 ml-4">Conferência Completa e Novidades na E3 2019: Microsoft</h3>
            <div class="row mt-3">
                <div class="container rounded h-100 sombraC text-center" style="background-image: url('images/Shirai/halow.jpg');background-size:cover;">
                    <iframe class="embed-responsive-item col-md-9 col-lg-9 p-0 mt-1 text-center sombraV" height="460px" src="https://www.youtube.com/embed/ixdUEbOI-38" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-lg-12">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class=" p-0 m-1 mb-3">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-8">
                                        <p class="mt-3">Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.</p>
                                        <p>Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                                </div>
                                            <div class="col-lg-4">
                                                <div class="col">
                                                    <h3 class="h3menor mt-4 bordah3">Páginas Relacionadass</h3>
                                                    <div class="container mt-3">
                                                    <h4><a href="lancamentostrailers.php">Novos Trailers Espetaculáres</a></h4>
                                                    <h4><a href="https://www.youtube.com/user/gamespot">Sony Traz Novidades</a></h4>
                                                    <h4><a href="https://www.youtube.com/user/gamespot">Mercado de Games Cresce</a></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            </div>    
                                        <div class="container-fluid rounded sombraC text-center col-lg-11" style="background-image: linear-gradient(-90deg, #0d0d0d,#212121, #212121, #0d0d0d);">
                                            
                                            <img src="images/Shirai/cyberpunk1.webp" class="rounded sombraV bordaimg mb-2 text-center mt-2" alt="teste" style="max-width: 100%;" />
                                        </div>
                                        <h3 class="mt-3">CyberPunk 2077</h3>
                                        <p class="mt-3 col-lg-11 p-0">
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                        <p class="col-lg-11 p-0">Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                        <div class="container-fluid rounded sombraC text-center col-lg-11" style="background-image: linear-gradient(-90deg, #0d0d0d,#212121, #212121, #0d0d0d);">
                                        <img src="images/Shirai/lastofus.webp" class="rounded  sombraV bordaimg mb-2 mt-2" alt="teste" style="max-width: 100%;" />
                                        </div>
                                        <h3 class="mt-3">The Last Of Us: Parte II</h3>
                                         <p class="col-lg-11 p-0 mt-3">
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                        <p class="col-lg-11 p-0">Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                            Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                                            cachacis, eu reclamis.
                                        </p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <section class="container"> 
                <hr class="hr">
                <h3 class="mt-3">Notícias Que Podem lhe Interessar</h3>
                <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 mr-1">
                                        <img src="images/Shirai/vingadores.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%;" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Novos Trailers do game dos Vingadores</a></h3>
                                            <p>A estúdio Square Enix está encarregado de trazer os vingadores ao mundo dos consoles, e ontem eles lançaram alguns trailers que deixou a galera na Conferência da E3 de boaca aberta!... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 ml-1">
                                        <img src="images/Shirai/fifa20.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Fifa 2020 Traz Novidades</a></h3>
                                            <p>Como já era de se esperar, um novo jogo do Fifa foi anunciado nesta E3, porém, o que deixou todos de queixo
                                                caído foi algumas novidades acrescentadas ao game... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            <div class="col-lg-4 fundosec">
                                <div class="text-center p-0 mt-1 mb-1 ml-1">
                                        <img src="images/Shirai/borderlands.webp" class="sombraV rounded img-thumbnail" style="max-width: 100%" alt="yeah" >
                                    <h3 class="mt-3"><a href="#">Borderlands 3 Anunciado</a></h3>
                                            <p>Em meio a tantos lançamentos esperados para o ano de 2019, tivemos uma bela supresa na E3 realizada no dia 13 de Junho, sexta-feira, que foi o anúncio do terceiro game da franquia Borderlands... <a href="#">Saiba Mais</a>
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>