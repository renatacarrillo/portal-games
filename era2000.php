<?php  
 // TRADUZ A PÁGINA 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="gamestuff">
    <link rel="icon" href="images/favicon3.png">
    <title>Gamestuff &#8226; Era 2000</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mine.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


</head>
<div class="color-background-gradient">

    <body class="color-background-gradient">

        <header>
        <?php
            include('includes/header.php');
        ?>
            <div class="videogames">
                <?php                    
                    echo '<br>';
                    // breadcrumb
                    breadcrumb(array('index.php'=>'Home', 'SITE - V 1.9/videogameshome.php'=>'Video Games Home', 'SITE - V 1.9/era8090.php' => 'Era 80 e 90', '' => 'Era 2000'));
					
					include('includes/lang.php'); 
                ?>
            </div>

        </header>

 
         <div class="container"><hr class="hr"></div>
       
        <div class="jumbotron p-3 p-md-5 text-black rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 >Lorem Ipsu</h1>
          <p>Lorem ipsum porta et maecenas ullamcorper vulputate egestas pulvinar at inceptos porttitor iaculis mollis netus vehicula posuere, aliquet cras purus magna adipiscing sociosqu suscipit amet ultricies nibh lorem elementum varius ultricies. molestie quisque nibh elementum convallis laoreet.  </p>
            <img class="#" src="images/Anderson/gameshome1.webp" alt="Foto  ps1 e N64" width="400" height="270">
         <a href="https://pt.wikipedia.org/wiki/Consoles_de_videogame_de_s%C3%A9tima_gera%C3%A7%C3%A3o" target="_blank"></a>
          </div>
        </div>
     
      <div class="row mb-2">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <h3 class="mb-0">
                <a class="text-dark" href="#">Lorem</a>
              </h3>
              <p class="card-text mb-auto">Lorem ipsum porta et maecenas ullamcorper vulputate egestas pulvinar at inceptos porttitor iaculis mollis netus vehicula posuere, aliquet cras purus magna adipiscing sociosqu suscipit amet ultricies nibh lorem elementum varius ultricies. </p>
              <a href="https://www.nerdssauros.com.br/2018/11/06/a-era-3d-a-5a-geracao-de-consoles/" class="text-blue font-weight-bold" target="_blank">Continue lendo</a>
            </div>
            <img class="card-img-right " src="images/Anderson/games3.webp"alt="Card image cap" width="250" height="300" target="_blank">
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <h3 class="mb-0">
                <a class="text-dark" href="#">Lorem</a>
              </h3>
              <p class="card-text mb-auto">Lorem ipsum porta et maecenas ullamcorper vulputate egestas pulvinar at inceptos porttitor iaculis mollis netus vehicula posuere, aliquet cras purus magna adipiscing sociosqu suscipit amet ultricies nibh lorem elementum varius ultricies. </p>
              <a href="https://pt.wikipedia.org/wiki/Consoles_de_videogame_de_oitava_gera%C3%A7%C3%A3o" class="text-blue font-weight-bold" target="_blank">Continue lendo</a>
            </div>
            <img class="card-img-right " src="images/Anderson/games2.webp"alt="Card image cap" width="250" height="300">
          </div>
        </div>
      </div>
        <footer>
            <?php  
                include('includes/footer.php'); 
            ?>
        </footer>
    </body>
	</div>
</div>

</html>