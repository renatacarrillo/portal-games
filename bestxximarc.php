<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 01 do tema de 'Jóogos Marcantes do Século XXI', contendo informações sobre os games que mais marcaram desde o ano 2000!">
    <meta name="author" content="VINÍCIUS LESSA | @contato: vinícius.lessa33@outlook.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>
    <div class="bestxxi">
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');
                            
                    echo '<br>';
                    // breadcrumb
                    if (isset($_POST['PT'])) {
                        breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/bestxxihome.php' =>'Melhores - Ranking' , '' =>'Melhores Games do Século XXI'));
                    } elseif (isset($_POST['EN'])) {
                        breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/bestxxihome.php' =>'Top Five - Games' , '' =>'Most Outstanding Games'));
                        
                    } else {
                        breadcrumb(array('index.php' => 'Home', 'SITE - V 2.0/bestxxihome.php' =>'Melhores - Ranking' , '' =>'Melhores Games do Século XXI'));
                    }

                    include('includes/lang.php');  
            ?>
            </div>
        </header>

        <article class="container">
            <hr class="hr">
            <div class="row mt-5">
                <div class="col-lg-12">
                    <div>
                        <h1 class="mb-1 text-center"><?php echo $aLang[15]; ?></h1>
                        <h4 class="mb-4 text-center"><i>"<?php echo $aLang[20]; ?>"</i></h4><br>
                        <h3 class="mb-4"><?php echo $aLang[21]; ?></h3>
                        <p>
                            &nbsp Mussum Ipsum, cacilds vidis litro abertis. Interagi no mé, cursus quis,
                            vehicula ac nisi. Manduma pindureta quium dia nois paga. Todo mundo vê os
                            porris que eu tomo, mas ninguém vê os tombis que eu levo! Per aumento de
                            cachacis, eu reclamis.
                        </p>
                        <p>
                            &nbsp Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eleifend euismod
                            felis egestas aliquet. Curabitur et porttitor odio. Integer dolor massa, mollis et
                            suscipit non, dictum vel enim. Praesent non nunc aliquet, rutrum dui mollis, vehicula
                            tortor. Nunc nisi ante, pulvinar at enim ut, porttitor mollis sapien. Aliquam erat 
                            volutpat. Donec cursus tortor sapien, ac placerat quam venenatis vitae. Sed sodales 
                            diam et turpis varius efficitur. Quisque aliquam urna elit, in bibendum metus posuereac. 
                            Nam eget dictum turpis, quis mattis odio. Pellentesque maximus rhoncus mi, et eleifend
                            elit viverra eu. 
                        </p>
                        <p>
                            &nbsp Sed at nisl erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras leo metus,
                            pellentesque at diam tincidunt, eleifend consectetur velit. Class aptent taciti sociosqu ad litora
                            torquent per conubia nostra, per inceptos himenaeos. Vivamus posuere, erat vitae condimentum sodales, est mauris finibus enim, non ornare dui mauris nec nisi. Vestibulum non orci libero. In ac maximus enim, vitae commodo odio. Aliquam a sem in diam fringilla porta. Quisque eu eleifend justo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris mauris augue, molestie vitae risus id, scelerisque aliquet est. Phasellus a egestas lectus, id tempor sem. Aliquam nulla nisi, tempor sed risus sed, bibendum fringilla lorem. Praesent congue elit a blandit porttitor. Nulla varius ultrices lacus in molestie. 
                        </p>
                    </div>
                </div>
            </div>

            <section class="container pt-3 pl-0 pr-0" style="background:">
                <div class="col-12 mb-4">
                    <hr class="hr mb-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col blockimg allimg gtasa rounded">
                                <img src="images/reddead1.webp" class="imgarticle" alt="Gta San Andreas Game"/>
                            </div>    
                            <div class="col pt-2 p-0">
                                <h3 class="text-center">GTA: SAN ANDREAS</h3>
                                <p>
                                    &nbsp Groove Street, assim como há 10 anos atrás, ainda é o lar do personagem CJ, Carl Johnson, 
                                    protagonista do game, que retorna a Los Santos (uma versão genérica de Los Angeles) depois de 
                                    saber da morte de sua mãe. Em contato com os velhos amigos, e seu irmão, Sweet, ele volta a se 
                                    envolver com o mundo do crime e das batalhas de gangue.Groove Street, assim como há 10 anos atrás, ainda é o lar do personagem CJ, Carl Johnson,
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col blockimg allimg thewitcher rounded">
                                <img src="images/reddead1.webp" class="imgarticle" alt="Gta San Andreas Game"/>
                            </div>    
                            <div class="col pt-2 p-0">
                                <h3 class="text-center">THE WITCHER 3</h3>
                                <p>
                                    &nbsp The Witcher 3: Wild Hunt, como citamos, vem para finalizar a saga de Geralt. Isso 
                                    não vai impedir de que saiam novos “The Witcher” ao longo dos próximos anos, mas Geralt
                                    de Rivia vai terminar sua história por aqui, de forma bem inspirada e com momentos épicos
                                    – dignos de grandes RPGs e similares.
                                    Mas o fim não vai ser algo necessariamente agradável, como uma bela aposentadoria, e sim 
                                    uma tarefa árdua que vai chegar ao bruxo quando menos ele espera.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col blockimg allimg lastofus rounded">
                                <img src="images/reddead1.webp" class="imgarticle" alt="Gta San Andreas Game"/>
                            </div>    
                            <div class="col pt-2 p-0">
                                <h3 class="text-center">THE LAST OF US</h3>
                                <p>
                                    &nbsp O enredo de The Last of Us é um excelente exemplo de que os jogos podem também contar
                                    uma história digna de uma obra literária. A trama gira em torno de Joel, que depois de perder sua filha tragicamente, precisa encontrar 
                                    motivações para viver em um mundo dominado pelo caos, depois de uma contaminação em massa, que transforma seres humanos em criaturas horripilantes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pr-2 pl-2 pt-4 pb-4 m-0 mb-5" style="background: rgba(0,0,0,0.7);">
                    <div class="col-lg-5 pl-1 pb-0 pt-2">
                        <h3 class="text-center">THE ELDER SCROLLS V:</h3>
                        <div class="m-0 mb-3 mt-4 p-0">
                            <p style="margin-bottom: -15px; padding-bottom: 0px;">
                                &nbsp Se existe uma frase que possa definir o jogo The Elder Scrolls V: Skyrim
                                , essa frase é: “Diga adeus à sua vida real”, e não há uma definição melhor para
                                ele, uma vez que você mergulha na história e começa a se aventurar em mais um
                                pedaço do reino de Tamriel.
                                Trata-se de uma aventura contínua, um mundo vasto e que transmite uma coisa não
                                muito comum em NPCs (personagens não jogáveis) do cenário: Vida. As pessoas nas
                                cidades trabalhando, e indo para suas casas para uma boa noite de sono, uma 
                                rotina que se repete dia após dia dentro do jogo, e uma grande interação com
                                o jogador.
                            </p>
                            <a target="blank" href="https://pt.wikipedia.org/wiki/The_Elder_Scrolls_V:_Skyrim"><br>Saiba Mais</a>
                        </div>
                    </div>
                    
                    <div class="col-lg-7 blockimg allimg skyrim rounded p-0" style="background-position: 30%;">
                        <img src="images/skyrim1.webp" class="imgarticle h-100" alt="Skyrim"/>
                    </div>
                    
                        
                </div>
            </section>
            <aside>
                <div class="row pt-4 pb-4 m-0 mb-5" style="background: rgba(0,0,0,0.7);">
                    <div class="col-12">
                        <h3 class="text-center">VEJA TAMBÉM:</h3>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="row">
                            <div class="col-md-4">
                                <ul>
                                    <li class="p-0 m-0"><a href="bestxxihome.php">TOP FIVE - MEHORES GAMES DO SÉCULO</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <li class=" p-0 m-0"><a href="bestxximarc.php">GAMES MAIS CARCANTES DO SÉCULO</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <li class=" p-0 m-0"><a href="bestxxiyear.php">PRÊMIO DE MELHORES JOGOS DO ANO</a></li>
                                </ul>                    
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <br>
        </article>       
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>