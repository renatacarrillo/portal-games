
<?php  
 // TRADUZ A PÁGINA 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<?php
    include('includes/header.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="author" content="gamestuff">
    <link rel="icon" href="images/favicon3.png">
    <title>Gamestuff &#8226; Video Games</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mine.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
 
  </head>
    <div class="color-background-gradient">
    <body class="color-background-gradient videogames">

    <header>
     <div class="videogames">
        <?php                    
          echo '<br>';
          // breadcrumb
          breadcrumb(array('index.php'=>'Home', ''=>'Video Games Home'));
		  
		  include('includes/lang.php'); 
        ?>
    </div>
      
    </header>

      <section>
        <div class="container">
          <div class="row">
            <div class="col-12">
              <hr class="hr">
              <h1 class="jumbotron-heading">Lorem</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <img class="imganderson" src="images/Anderson/magnavox.webp" alt="foto primeiro video-game da história" width="550" height="400">
              <p>
                <a href="https://pt.wikipedia.org/wiki/Magnavox_Odyssey"  class="btn btnanderson btnanderson btn-primary my-2">Saiba Mais</a>
              </p>
            </div>
            <div class="col-lg-6">           
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
            </div>
          </div>
        </div>
      </section>

      <div class="album py-3" >
        <div class="container">

          <div class="row">
            <div class="col-md-4 ">
              <div class="card" >
                <img class="card-img-top" src="images/Anderson/ralph.webp" alt="Foto criador magnavox odssey" width="100" height="200">
                <div class="card-body" >
                  <p class="card-text" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                  <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                      <a href="era8090.php"  class="btn btnanderson btn-primary my-2">Anos 80</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="images/Anderson/pong.webp" alt="Foto jogo pong" width="100" height="200">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                    <a href=https://pong-2.com/  class="btn btnanderson btn-primary my-2">Play</a>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
               <img class="card-img-top" src="images/Anderson/promo.webp" alt="Foto jogo pong" width="100" height="200">
                <div class="card-body">
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at risus neque. Cras sit amet ligula ut justo commodo porta id ut enim. Nulla est lectus, mollis sit amet vehicula id, volutpat eget mauris.</p>
                
                
                  <a href="https://www.youtube.com/watch?v=_L9D70OARm4"  class="btn btnanderson btn-primary my-2"aria-label="Left Align">Link</a>
                <a href="https://www.youtube.com/watch?v=WPLjE_LzvDc"  class="btn btnanderson btn-primary my-1">Link</a>
                <a href="https://www.youtube.com/watch?v=L9Hnxe6ejNM"  class="btn btnanderson btn-primary my-2">Link</a>
                     
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
     
          <?php
            include('includes/footer.php'); 
          ?>
    
        </div>            
  </body>
        </div>
</html>
