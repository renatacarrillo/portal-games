<!-- TRADUÇÃO da PÁGINA -->
<?php
include('includes/process.php');

if (isset($_POST['PT'])) {
    $lang = $_POST['PT'];
    $aLang = Translate($lang);
} elseif (isset($_POST['EN'])) {
    $lang = $_POST['EN'];
    $aLang = Translate($lang);
} else {
    $aLang = $_SESSION['LANG'];
}
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II: página 03 do tema de 'Mobile e Portáteis'">
    <meta name="author" content="LUCAS JUSTI | @contato: lucas.silva563@fatec.sp.gov.br">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>A evolução dos portáteis.</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>
    <div class="mobilereleases">
        <header>
            <div>
                <?php
                // incluindo topo da página 
                include('includes/header.php');

                echo '<br>';
                // breadcrumb
                breadcrumb(array('index.php' => 'Home', '' => 'Evolução dos Portáteis'));

                include('includes/lang.php');
                ?>
            </div>
        </header>
        <main class="container">
            <hr class="hr">
            <article>
                <div class="row">
                    <div class="col-12 mt-5 mb-4 text-center">
                        <h1 class="text-center"><?php echo $aLang['107']; ?></h1>
                        <p class="lead text-center"><?php echo $aLang['108']; ?></p>
                        <img src="images/Lucas/handheld.webp" class="img-fluid" alt="Consoles portáteis alinhados horizontalmente." height="104" width="600">
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1979 - Milton Bradley Microvision</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">1981 - Entex Select-A-Game Machine</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eRqYa6yL8Jk" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1984 - Epoch Game Pocket Computer</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">1989 - Nintendo Game Boy</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/E-ej_8XBwmI" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1989 - Atari Lynx</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">1990 - NEC TurboExpress</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wxpsBsupgUo" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1990 - Sega Game Gear</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">1995 - Sega Genesis Nomad</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1997 - Tiger Game.com</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">1998 - Nintendo Game Boy Color</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/nxnc9qBWffQ" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">1999 - SNK Neo Geo Pocket Color</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">2000 - Bandai WonderSwan Color</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7WpIRAEfnrg" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">2001 - Nintendo Game Boy Advance</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">2001 - Game Park GP32</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/beDmGfLL9UA" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">2003 - Nokia N-Gage</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">2004 - Sony PlayStation Portable</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">2004 - Nintendo DS</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">2005 - Tiger Telematics Gizmondo</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col mt-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UtMpA8PFiXw" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">2009 - PSP Go</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4">
                        <h2 class="text-left">2011 - Nintendo 3DS</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6 mt-4">
                        <h2 class="text-left">2011 - PlayStation Vita</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                    <div class="col col-lg-6 mt-4 mb-5">
                        <h2 class="text-left">2017 - Nintendo Switch</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div>
            </article>
            <section class="container mobileTheme">
                <div class="row mt-5">
                    <div class="col">
                        <h3 class="text-center mt-2"><?php echo $aLang['102']; ?></h3>
                        <hr class="hr">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-5 text-center">
                        <a href="mobileranking.php">
                            <h4><?php echo $aLang['105']; ?></h4>
                        </a>
                        <a href="mobileranking.php"><img src="images/Lucas/ranking.webp" alt="numeros dispostos em forma de escada" width="200" height="200"></a>
                    </div>
                    <div class="col-md-6 mb-5 text-center">
                        <a href="mobilereleases.php">
                            <h4><?php echo $aLang['100']; ?></h4>
                        </a>
                        <a href="mobilereleases.php"><img src="images/Lucas/releases.webp" alt="personagem Mario Bros dirigindo um kart" width="200" height="200"></a>
                    </div>
                </div>
            </section>
        </main>
        <!--Footer-->
        <?php
        // incluindo footer da página
        include('includes/footer.php');
        ?>
    </div>
</body>

</html>