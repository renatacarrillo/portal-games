<!-- TRADUÇÃO da PÁGINA -->
<?php 
    include('includes/process.php');
    
    if (isset($_POST['PT'])) {
        $lang = $_POST['PT']; 
        $aLang = Translate($lang);  
    } elseif (isset($_POST['EN'])) {
        $lang = $_POST['EN']; 
        $aLang = Translate($lang);   
    } else {
        $aLang = $_SESSION['LANG'];
    }
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
        content="FATEC São Roque - 2019 - PROJETO INTEGRADOR II">
    <meta name="author" content="Sthefany Pereira Nolasco de Souza | @contato: sthefany.nolasco22@gmail.com">
    <!-- FONTES CDN: 
        - https://fonts.google.com/
        - https://fontawesome.com/
    -->
    <link rel="icon" href="images/favicon3.png">
    <title>Melhores Games do Século XXI</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CSS padrão -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- icones footer -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body class="vrgames">
    <div>
        <header>
            <div>
                <?php
                 // incluindo topo da página 
                    include('includes/header.php');   
                            
                    echo '<br>';

                    // breadcrumb
                    if (isset($_POST['PT'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simuladores', '' => 'Simuladores PC'));
                    } elseif (isset($_POST['EN'])) {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simulator Games', '' => 'Simulators for PC'));
                    } else {
                        breadcrumb(array('index.php' => 'Página Inicial', 'SITE - V 2.1/vrgameshome.php' =>'Simuladores', '' => 'Simuladores PC'));
                    }
                    
                    include('includes/lang.php');  
            ?>
            </div>
        </header>

        <main class="container">
        <hr class="hr">
        <article>
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="text-center mt-5"><?php echo $aLang['25']; ?></h1>
                    <p class="lead text-center mb-4">Os 10 melhores jogos de realidade virtual para PC.</p>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Superhot VR</h2>
                        <p>Superhot VR é um dos jogos de realidade virtual para PC de maior popularidade. O game é totalmente independente e possui uma premissa muito simples: o tempo só prossegue quando você se movimenta. Caso seu personagem fique totalmente parado, o tempo, as pessoas e os objetos ao seu redor também param.

                        Considerando que o jogo está repleto de inimigos querendo te matar, você pode usar isso ao seu favor para desviar de tiros e acertar seus oponentes na hora certa.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IRtMkqIjM30" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Keep Talking and Nobody Explodes</h2>
                        <p>O Keep Talking and Nobody Explodes até pode ser jogado sem um dispositivo de realidade virtual, mas seu conceito faz mais sentido se você tiver usando o acessório. Ele coloca o jogador principal em frente a uma bomba que precisa ser desarmada. Enquanto isso, os outros jogadores não podem olhar para a bomba, mas ficam com as instruções de como desativar os dispositivos aleatórios que podem aparecer nela.

                        Com um dispositivo de RV, você precisa detalhar exatamente o que aparece na bomba para receber essas instruções e fazer o possível para desativá-la a tempo. O game é bem desafiante e extremamente divertido.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/kkdbzFV1NoQ" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Alien: Isolation</h2>
                        <p>Seja um fã dos filmes da franquia Alien ou não, o game Alien: Isolation é um excelente exemplo de como fazer um game de terror realmente assustador em dispositivos de realidade virtual. Em vez de apelar para sustos aleatórios, o jogo cria uma atmosfera bem tensa, já que você precisa se esconder dos Aliens que te caçam em sua própria nave.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7h0cgmvIrZw" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Ghost Giant</h2>
                        <p>Uma aventura de puzzles encantadora do estúdio responsável por sucessos indie como o Fe e o Stick It To The Man. Entra na pele do gigantesco e fantasmagórico protetor de um rapaz pequeno e solitário chamado Louis. Juntos, enfrentarão todo o tipo de quebra-cabeças e ajudarão os cidadãos de Sancourt.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hEJnJz43e10" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">I Expect You to Die</h2>
                        <p>O jogo I Expect You to Die te coloca naquelas situações clichês de filmes do James Bond, sendo que você está preso em um carro cheio de armadilhas e precisa encontrar uma maneira de escapar. O desafio é resolver e se livrar de cada uma das armadilhas, já que só depois disso é possível fugir com segurança. O legal é que há várias fases diferentes, cada uma com desafios cada vez mais complexos.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vedFA8yXr2Y" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Elite: Dangerous</h2>
                        <p>Embora tenha sido lançado antes da maioria dos dispositivos de realidade virtual terem se popularizado, Elite: Dangerous oferece total suporte para este modo de jogo e fica bem mais divertido e imersivo desta forma.

                        Ele consiste de um tipo de aventura espacial, sendo que você pode escolher entre classes bem diferentes para explorar a galáxia. Seja para jogar como comerciante, pirata espacial ou até um caçador de recompensas, o título vale bastante a pena</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YESNObZJTgQ" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">Thumper</h2>
                        <p>Thumper é um tipo de jogo musical com certa inspiração de franquias como Guitar Hero e Rock Band, mas com uma proposta e visual totalmente diferentes do convencional. Nele, você controla um tipo de besouro metálico que passa por cenários meio psicodélicos em alta velocidade.

                        No meio do caminho, você encontrará paredes em que pode deslizar e obstáculos que precisa evitar. Tudo acontece de maneira rápida, mas são essas combinações que geram diversos tipos de músicas originais.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/gtPGX8i1Eaw" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Rez Infinite</h2>
                        <p>Outra boa opção para quem gosta de jogos musicais é o Rez Infinite. Ele funciona basicamente como um shooter espacial em terceira pessoa, no qual você precisa destruir todos os inimigos que aparecem na sua frente.

                        A diferença é cada movimento e cada tiro contribui para a trilha sonora do jogo, o que o torna bem único e bem divertido. O game em si é bem antigo, mas foi relançado para PCs há algum tempo e possui total suporte para dispositivos de realidade virtual.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/c7zjVA6KsmY" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <h2 class="">The Climb</h2>
                        <p>he Climb também possui um conceito bem simples, mas que funciona perfeitamente como um dos jogos de realidade virtual para PC. Nele, você se encontra em uma montanha que precisa escalar até topo com segurança.

                        Há diversas rotas que você pode usar, sendo que cada uma tem suas vantagens e perigos que devem ser calculados com bastante cuidado e certa estratégia.  </p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vJYW7YOssFI" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <div class="row invert">
                    <div class="col-12 col-md-6">
                        <h2 class="">Hover Junkers</h2>
                        <p>Se fosse um mero título de PC, este game multiplayer de tiro em primeira pessoa possivelmente não se destacaria tanto, mas por ter suporte nativo para dispositivos de realidade virtual, a situação é bem diferente.

                        Ele oferece uma experiência bem intensa, imersiva e divertida, que você pode aproveitar com seus amigos ou jogadores desconhecidos do mundo todo. Basicamente, seu personagem se encontra em um mundo pós-apocalíptico e é necessário batalhar contra inimigos para conseguir suprimentos importantes.</p>
                    </div>
                    <div class="col col-md-6">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/jZnZrcvvyZM" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

            </div>
        </article>
    </main>          
    </div>
    <!--Footer-->
    <?php
	// incluindo footer da página
	include('includes/footer.php');
    ?>
</body>

</html>